<?php
    namespace MyProject\Models\Comments;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\Models\ActiveRecordEntity;

    class Comment extends ActiveRecordEntity{
        protected $name;
        protected $text;
        protected $idArticle;
        protected $createdAt;

        public static function getTableName(): string{
            return 'comments';
        }
        public function getText(){
            return $this->text;
        }public function getName(){
            return $this->name;
        }
        public function setName(string $name){
            $this->name = $name;
        }
        public function setText(string $text){
            $this->text = $text;
        }
        public function setAuthorId(User $author){
            $this->authorId = $author->id;
        }
        public function setIdArticle(string $idArticle){
            $this->idArticle = $idArticle;
        }
    }
?>