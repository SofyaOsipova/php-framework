<?php
    namespace MyProject\Controllers;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\View\View;
    use MyProject\Models\Comments\Comment;

    class CommentController{
        private $view;
        private $db;

        public function __construct(){
            $this->view = new View(__DIR__.'/../../../templates');
        }
        public function comment(int $idArticle){
            $comments = Comment::getByArticleId($idArticle);
            if ($comments[0] === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('comments/comment.php', ['comments' => $comments]);
        }

        public function addComment(int $idArticle):void {
            $comment = new Comment();
            $comment->setIdArticle($idArticle);
            $comment->setAuthorId(User::getById(1));
            $comment->setName('user');
            if (isset($_POST['content'])) {
                $comment->setText($_POST['content']);
            } else {
                $comment->setText('new comment');
            }
            $comment->save();
        }

        public function edit(int $id): void {
            $comment = Comment::getById($id);
            if ($comment === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('comments/edit.php', ['comment' => $comment]);
        }

        public function editComment(int $id, int $commentId): void {
            $comment = Comment::getById($commentId);
            if ($comment === null) {
                $this->view->renderHTML('errors/404.php', [], 404);
                return;
            }
            if (isset($_GET['new-content'])) {
                $comment->setText($_GET['new-content']);
            } else {
                $comment->setText('new comment');
            }
            $comment->save();
        }
    }
?>
